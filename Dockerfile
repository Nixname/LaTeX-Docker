FROM debian@sha256:1e61bfbfcef8f9690a0641e4dbb0eae46e2ad00eff065bd586a1d58967ee4b66
# FROM debian:buster-20210329-slim

RUN apt-get update && apt-get install -y \
		biber=2.12-2 \
		latexmk=1:4.61-0.1 \
		make=4.2.1-1.2 \
		texlive-full=2018.20190227-2 \
	&& rm -rf /var/lib/apt/lists/*
