# Container based LaTeX compilation

This repository aims to specify dependencies and their versions as accurately as possible allowing reproducible builds. Furthermore moving tags will be kept at a minimum to allow users of this repository precise version specifications.

- [Git-Repository](https://git.sr.ht/~nixname/LaTeX-Docker)
- [Docker-Repository](https://hub.docker.com/r/nixname5/latex)
